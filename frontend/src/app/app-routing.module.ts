import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from './containers/home/home-page/home-page.component';
import {LoginPageComponent} from './containers/auth/login-page/login-page.component';
import {AuthGuard} from './core/guards/auth.guard';
import {PatientPageComponent} from './containers/patients/patient-page/patient-page.component';
import {PatientEditPageComponent} from './containers/patients/patient-edit-page/patient-edit-page.component';
import {PatientCreatePageComponent} from './containers/patients/patient-create-page/patient-create-page.component';
import {CaregiverPageComponent} from './containers/caregivers/caregiver-page/caregiver-page.component';
import {CaregiverEditPageComponent} from './containers/caregivers/caregiver-edit-page/caregiver-edit-page.component';
import {CaregiverCreatePageComponent} from './containers/caregivers/caregiver-create-page/caregiver-create-page.component';
import {MedicationPageComponent} from './containers/medications/medication-page/medication-page.component';
import {MedicationEditPageComponent} from './containers/medications/medication-edit-page/medication-edit-page.component';
import {MedicationCreatePageComponent} from './containers/medications/medication-create-page/medication-create-page.component';
import {MedicationPlanDoctorPageComponent} from './containers/medication-plan/medication-plan-doctor-page/medication-plan-doctor-page.component';
import {MedicationPlanPatientPageComponent} from './containers/medication-plan/medication-plan-patient-page/medication-plan-patient-page.component';
import {MedicationPlanCreatePageComponent} from './containers/medication-plan/medication-plan-create-page/medication-plan-create-page.component';
import {CaregiverAttachPatientPageComponent} from './containers/caregivers/caregiver-attach-patient-page/caregiver-attach-patient-page.component';
import {CaregiverPatientPageComponent} from './containers/caregivers/caregiver-patient-page/caregiver-patient-page.component';
import {PatientPersonalAccountPageComponent} from './containers/patients/patient-personal-account-page/patient-personal-account-page.component';
import {PatientNotificationsPageComponent} from './containers/caregivers/patient-notifications-page/patient-notifications-page.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'login', component: LoginPageComponent},
  {path: 'home', component: HomePageComponent, canActivate: [AuthGuard]},
  {path: 'patients', component: PatientPageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {path: 'patients/:id/edit', component: PatientEditPageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {path: 'patients/create', component: PatientCreatePageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {path: 'caregivers', component: CaregiverPageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {path: 'caregivers/:id/edit', component: CaregiverEditPageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {path: 'caregivers/:id/attach', component: CaregiverAttachPatientPageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {path: 'caregivers/create', component: CaregiverCreatePageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {path: 'medications', component: MedicationPageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {path: 'medications/:id/edit', component: MedicationEditPageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {path: 'medications/create', component: MedicationCreatePageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {path: 'medications/plans/doctor', component: MedicationPlanDoctorPageComponent, canActivate: [AuthGuard], data: {roles: 'DOCTOR'}},
  {
    path: 'medications/plans/doctor/create',
    component: MedicationPlanCreatePageComponent,
    canActivate: [AuthGuard],
    data: {roles: 'DOCTOR'}
  },
  {path: 'medications/plans/patient', component: MedicationPlanPatientPageComponent, canActivate: [AuthGuard], data: {roles: 'PATIENT'}},
  {path: 'me', component: PatientPersonalAccountPageComponent, canActivate: [AuthGuard], data: {roles: 'PATIENT'}},
  {path: 'caregivers/patients', component: CaregiverPatientPageComponent, canActivate: [AuthGuard], data: {roles: 'CAREGIVER'}},
  {path: 'caregivers/notifications', component: PatientNotificationsPageComponent, canActivate: [AuthGuard], data: {roles: 'CAREGIVER'}},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
