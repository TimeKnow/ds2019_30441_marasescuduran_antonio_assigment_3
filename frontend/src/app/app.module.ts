import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {appReducers} from './store/reducers/app.reducers';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDividerModule} from '@angular/material/divider';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatListModule} from '@angular/material/list';
import {MAT_DIALOG_DATA, MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatStepperModule} from '@angular/material/stepper';
import {MatChipsModule} from '@angular/material/chips';
import {MatBadgeModule} from '@angular/material/badge';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatMenuModule} from '@angular/material/menu';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTreeModule} from '@angular/material/tree';
import {LoginFormComponent} from './components/auth/login-form/login-form.component';
import {AuthEffects} from './store/effects/auth.effects';
import {LoginPageComponent} from './containers/auth/login-page/login-page.component';
import {HomePageComponent} from './containers/home/home-page/home-page.component';
import {MenuDisplayComponent} from './components/util/menu-display/menu-display.component';
import {MenuHandlerComponent} from './containers/util/menu-handler/menu-handler.component';
import {PatientEffects} from './store/effects/patient.effects';
import {PatientPageComponent} from './containers/patients/patient-page/patient-page.component';
import {PatientListComponent} from './components/patients/patient-list/patient-list.component';
import {LoadingDisplayComponent} from './components/util/loading-display/loading-display.component';
import {HomeButtonCardComponent} from './components/home/home-button-card/home-button-card.component';
import {PatientListActionMenuComponent} from './components/patients/patient-list-action-menu/patient-list-action-menu.component';
import {CaregiverEffects} from './store/effects/caregiver.effects';
import {MedicationEffects} from './store/effects/medication.effects';
import {PatientFormComponent} from './components/patients/patient-form/patient-form.component';
import {PatientEditPageComponent} from './containers/patients/patient-edit-page/patient-edit-page.component';
import {PatientCreatePageComponent} from './containers/patients/patient-create-page/patient-create-page.component';
import {CaregiverListComponent} from './components/caregivers/caregiver-list/caregiver-list.component';
import {CaregiverListActionMenuComponent} from './components/caregivers/caregiver-list-action-menu/caregiver-list-action-menu.component';
import {CaregiverFormComponent} from './components/caregivers/caregiver-form/caregiver-form.component';
import {CaregiverPageComponent} from './containers/caregivers/caregiver-page/caregiver-page.component';
import {CaregiverEditPageComponent} from './containers/caregivers/caregiver-edit-page/caregiver-edit-page.component';
import {CaregiverCreatePageComponent} from './containers/caregivers/caregiver-create-page/caregiver-create-page.component';
import {MedicationListComponent} from './components/medications/medication-list/medication-list.component';
import {MedicationListActionMenuComponent} from './components/medications/medication-list-action-menu/medication-list-action-menu.component';
import {MedicationFormComponent} from './components/medications/medication-form/medication-form.component';
import {MedicationPageComponent} from './containers/medications/medication-page/medication-page.component';
import {MedicationEditPageComponent} from './containers/medications/medication-edit-page/medication-edit-page.component';
import {MedicationCreatePageComponent} from './containers/medications/medication-create-page/medication-create-page.component';
import {MedicationPlanFormComponent} from './components/medication-plan/medication-plan-form/medication-plan-form.component';
import {MedicationPlanListComponent} from './components/medication-plan/medication-plan-list/medication-plan-list.component';
import {MedicationPlanDoctorPageComponent} from './containers/medication-plan/medication-plan-doctor-page/medication-plan-doctor-page.component';
import {MedicationPlanEffects} from './store/effects/medication-plan.effects';
import {MedicationPlanCreatePageComponent} from './containers/medication-plan/medication-plan-create-page/medication-plan-create-page.component';
import {MedicationPlanPatientPageComponent} from './containers/medication-plan/medication-plan-patient-page/medication-plan-patient-page.component';
import {CaregiverAttachPatientFormComponent} from './components/caregivers/caregiver-attach-patient-form/caregiver-attach-patient-form.component';
import {CaregiverAttachPatientPageComponent} from './containers/caregivers/caregiver-attach-patient-page/caregiver-attach-patient-page.component';
import {CaregiverPatientPageComponent} from './containers/caregivers/caregiver-patient-page/caregiver-patient-page.component';
import {PatientPersonalAccountComponent} from './components/patients/patient-personal-account/patient-personal-account.component';
import {PatientPersonalAccountPageComponent} from './containers/patients/patient-personal-account-page/patient-personal-account-page.component';
import {myRxStompConfig} from './my-rx-stomp.config';
import {InjectableRxStompConfig, RxStompService, rxStompServiceFactory} from '@stomp/ng2-stompjs';
import { PatientNotificationsPageComponent } from './containers/caregivers/patient-notifications-page/patient-notifications-page.component';
import { PatientNotificationsListComponent } from './components/caregivers/patient-notifications-list/patient-notifications-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    LoginPageComponent,
    HomePageComponent,
    MenuDisplayComponent,
    MenuHandlerComponent,
    PatientPageComponent,
    PatientListComponent,
    LoadingDisplayComponent,
    HomeButtonCardComponent,
    PatientListActionMenuComponent,
    PatientFormComponent,
    PatientEditPageComponent,
    PatientCreatePageComponent,
    CaregiverListComponent,
    CaregiverListActionMenuComponent,
    CaregiverFormComponent,
    CaregiverPageComponent,
    CaregiverEditPageComponent,
    CaregiverCreatePageComponent,
    MedicationListComponent,
    MedicationListActionMenuComponent,
    MedicationFormComponent,
    MedicationPageComponent,
    MedicationEditPageComponent,
    MedicationCreatePageComponent,
    MedicationPlanFormComponent,
    MedicationPlanListComponent,
    MedicationPlanDoctorPageComponent,
    MedicationPlanCreatePageComponent,
    MedicationPlanPatientPageComponent,
    CaregiverAttachPatientFormComponent,
    CaregiverAttachPatientPageComponent,
    CaregiverPatientPageComponent,
    PatientPersonalAccountComponent,
    PatientPersonalAccountPageComponent,
    PatientNotificationsPageComponent,
    PatientNotificationsListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot([AuthEffects, PatientEffects, CaregiverEffects, MedicationEffects, MedicationPlanEffects]),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),
    MatInputModule, MatFormFieldModule, MatCardModule, MatTableModule, MatButtonModule, MatCheckboxModule,
    MatDividerModule, MatIconModule, MatGridListModule, MatListModule, MatSidenavModule, MatDialogModule,
    MatToolbarModule, MatDatepickerModule, MatSelectModule, FormsModule,
    MatNativeDateModule, MatExpansionModule, MatStepperModule, MatChipsModule, MatBadgeModule,
    MatAutocompleteModule, MatProgressSpinnerModule, MatMenuModule, MatTabsModule, MatTreeModule,
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}},
    {provide: MatDialogRef, useValue: {}},
    {provide: MAT_DIALOG_DATA, useValue: {}},
    {
      provide: InjectableRxStompConfig,
      useValue: myRxStompConfig
    },
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
      deps: [InjectableRxStompConfig]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
