import {Component, Input, OnInit} from '@angular/core';
import {Patient} from '../../../core/models/patient';

@Component({
  selector: 'app-patient-personal-account',
  templateUrl: './patient-personal-account.component.html',
  styleUrls: ['./patient-personal-account.component.css']
})
export class PatientPersonalAccountComponent {

  @Input() patient: Patient;

  constructor() {
  }

}
