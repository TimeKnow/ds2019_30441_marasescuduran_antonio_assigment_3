import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-patient-form',
  templateUrl: './patient-form.component.html',
  styleUrls: ['./patient-form.component.css']
})
export class PatientFormComponent {

  @Input() form: FormGroup;
  @Input() actionButtonText: string;
  @Output() actionEvent: EventEmitter<any> = new EventEmitter<any>();

}
