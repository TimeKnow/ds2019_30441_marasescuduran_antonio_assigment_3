import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Patient} from '../../../core/models/patient';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {
  @Input() dataList: Patient[];
  @Input() noActionMenu = true;
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  dataListHeaders = [];

  ngOnInit(): void {
    if (!this.noActionMenu) {
      this.dataListHeaders = ['id', 'email', 'name', 'birthDate', 'gender', 'address', 'medicalRecord', 'actions'];
    } else {
      this.dataListHeaders = ['id', 'email', 'name', 'birthDate', 'gender', 'address', 'medicalRecord'];
    }
  }

  onDelete(id: number) {
    this.delete.emit(id);
  }
}
