import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Patient} from '../../../core/models/patient';
import {Medication} from '../../../core/models/medication';
import {Observable, of} from 'rxjs';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';

@Component({
  selector: 'app-medication-plan-form',
  templateUrl: './medication-plan-form.component.html',
  styleUrls: ['./medication-plan-form.component.css']
})
export class MedicationPlanFormComponent {
  @ViewChild('medicationInput', {static: false}) medicationInput: ElementRef<HTMLInputElement>;
  @Input() form: FormGroup;
  @Input() actionButtonText: string;
  @Input() patientList: Patient[];
  @Input() medicationList: Medication[];
  @Output() actionEvent: EventEmitter<any> = new EventEmitter<any>();

  readonly selectable = true;
  readonly removable = true;
  filteredMedications$: Observable<Medication[]>;
  selectedMedications: Medication[] = [];

  filterAvailableResourcesByInput(): void {
    const input = this.medicationInput.nativeElement.value;
    if (!input) {
      this.filteredMedications$ = of(this.medicationList);
      return;
    }

    const searchedTerm = input;
    this.filteredMedications$ = of(this.medicationList.filter(x => x.name.toLowerCase().includes(searchedTerm.toLowerCase())));
  }

  remove(medication: Medication): void {
    this.selectedMedications = this.selectedMedications.filter(x => x.id !== medication.id);
    this.onFormDataChange();
  }

  selectedAutocompleteMedication(event: MatAutocompleteSelectedEvent): void {
    const medication: Medication = event.option.value;
    if (this.selectedMedications.includes(medication)) {
      return;
    }
    this.selectedMedications.push(medication);
    this.medicationInput.nativeElement.value = '';
    this.onFormDataChange();
  }

  onFormDataChange() {
    this.form.get('medicationListIds').setValue(this.selectedMedications.map(x => x.id));
  }
}
