import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Patient} from '../../../core/models/patient';
import {MedicationPlan} from '../../../core/models/medication-plan';

@Component({
  selector: 'app-medication-plan-list',
  templateUrl: './medication-plan-list.component.html',
  styleUrls: ['./medication-plan-list.component.css']
})
export class MedicationPlanListComponent {


  @Input() dataList: MedicationPlan[];
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  dataListHeaders = ['id', 'doctorId', 'patientId', 'intakeIntervals', 'period'];

  onDelete(id: number) {
    this.delete.emit(id);
  }
}
