import {Component, Input} from '@angular/core';
import CaregiverNotification from '../../../core/models/caregiver-notification';

@Component({
  selector: 'app-patient-notifications-list',
  templateUrl: './patient-notifications-list.component.html',
  styleUrls: ['./patient-notifications-list.component.css']
})
export class PatientNotificationsListComponent {
  @Input() dataList: CaregiverNotification[];
  dataListHeaders = ['patientId', 'message'];

  constructor() {
  }
}
