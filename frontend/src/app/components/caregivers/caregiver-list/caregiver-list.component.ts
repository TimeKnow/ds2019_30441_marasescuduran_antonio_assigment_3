import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Caregiver} from '../../../core/models/caregiver';

@Component({
  selector: 'app-caregiver-list',
  templateUrl: './caregiver-list.component.html',
  styleUrls: ['./caregiver-list.component.css']
})
export class CaregiverListComponent {
  @Input() dataList: Caregiver[];
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  dataListHeaders = ['id', 'email', 'name', 'birthDate', 'gender', 'address', 'actions'];

  onDelete(id: number) {
    this.delete.emit(id);
  }
}
