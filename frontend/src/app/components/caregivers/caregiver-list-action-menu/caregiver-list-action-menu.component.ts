import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-caregiver-list-action-menu',
  templateUrl: './caregiver-list-action-menu.component.html',
  styleUrls: ['./caregiver-list-action-menu.component.css']
})
export class CaregiverListActionMenuComponent {
  @Input() elementId: number;
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
}
