import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-medication-list-action-menu',
  templateUrl: './medication-list-action-menu.component.html',
  styleUrls: ['./medication-list-action-menu.component.css']
})
export class MedicationListActionMenuComponent {
  @Input() elementId: number;
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
}
