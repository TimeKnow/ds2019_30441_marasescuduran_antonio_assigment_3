import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-medication-form',
  templateUrl: './medication-form.component.html',
  styleUrls: ['./medication-form.component.css']
})
export class MedicationFormComponent {
  @Input() form: FormGroup;
  @Input() actionButtonText: string;
  @Output() actionEvent: EventEmitter<any> = new EventEmitter<any>();
}
