import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Patient} from '../../../core/models/patient';
import {MedicationPlan} from '../../../core/models/medication-plan';
import {Medication} from '../../../core/models/medication';

@Component({
  selector: 'app-medication-list',
  templateUrl: './medication-list.component.html',
  styleUrls: ['./medication-list.component.css']
})
export class MedicationListComponent {
  @Input() dataList: Patient[];
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  dataListHeaders = ['id', 'name', 'sideEffects', 'dosage', 'actions'];

  onDelete(id: number) {
    this.delete.emit(id);
  }
}
