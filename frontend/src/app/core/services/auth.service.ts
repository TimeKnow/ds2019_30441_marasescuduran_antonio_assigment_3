import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EmailCredential} from '../models/email-credential';
import {Observable} from 'rxjs';
import {AuthUser} from '../models/auth-user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    }),
    withCredentials: true
  };

  constructor(private http: HttpClient) {
  }

  login(credential: EmailCredential): Observable<{}> {
    const body = new URLSearchParams();
    body.append('email', credential.email);
    body.append('password', credential.password);
    return this.http.post('/api/login', body.toString(), this.httpOptions);
  }

  logout(): Observable<{}> {
    return this.http.post('/api/logout', '', this.httpOptions);
  }

  getCurrentUser(): Observable<AuthUser> {
    return this.http.get<AuthUser>('/api/me', this.httpOptions);
  }
}
