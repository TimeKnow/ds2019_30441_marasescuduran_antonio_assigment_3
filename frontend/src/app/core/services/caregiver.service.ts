import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Caregiver} from '../models/caregiver';
import {Patient} from '../models/patient';

@Injectable({
  providedIn: 'root'
})
export class CaregiverService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    withCredentials: true
  };

  constructor(private http: HttpClient) {
  }

  getAllCaregivers(): Observable<Caregiver[]> {
    return this.http.get<Caregiver[]>('api/caregivers', this.httpOptions);
  }

  getCaregiverById(id: number): Observable<Caregiver> {
    return this.http.get<Caregiver>('api/caregivers/' + id, this.httpOptions);
  }

  deleteCaregiver(id: number): Observable<{}> {
    return this.http.delete('api/caregivers/' + id, this.httpOptions);
  }

  updateCaregiver(id: number, caregiver: Caregiver): Observable<Caregiver> {
    return this.http.put<Caregiver>('api/caregivers/' + id, caregiver, this.httpOptions);
  }

  createCaregiver(caregiver: Caregiver): Observable<Caregiver> {
    return this.http.post<Caregiver>('api/caregivers', caregiver, this.httpOptions);
  }

  getPatientsOfCaregiver(caregiverId: number): Observable<Patient[]> {
    return this.http.get<Patient[]>('api/caregivers/' + caregiverId + '/patients', this.httpOptions);
  }

  attachPatientToCaregiver(caregiverId: number, patientId: number): Observable<Patient> {
    return this.http.post<Patient>('api/caregivers/' + caregiverId + '/patients/' + patientId, this.httpOptions);
  }
}
