import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MedicationPlan} from '../models/medication-plan';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MedicationPlanService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    withCredentials: true
  };

  constructor(private http: HttpClient) {
  }

  getAllMedicationPlans(): Observable<MedicationPlan[]> {
    return this.http.get<MedicationPlan[]>('api/medications/plans', this.httpOptions);
  }

  getAllMedicationPlansForPatient(id: number): Observable<MedicationPlan[]> {
    return this.http.get<MedicationPlan[]>('api/medications/plans/patient/' + id, this.httpOptions);
  }

  createMedicationPlan(medicationPlan: MedicationPlan): Observable<MedicationPlan> {
    return this.http.post<MedicationPlan>('api/medications/plans', medicationPlan, this.httpOptions);
  }
}
