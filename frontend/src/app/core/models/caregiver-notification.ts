export default class CaregiverNotification {
  patientId: number;
  message: string;
}
