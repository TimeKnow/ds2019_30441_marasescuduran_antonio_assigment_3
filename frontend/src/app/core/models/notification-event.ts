import CaregiverNotification from './caregiver-notification';

export class NotificationEvent {
  type: string;
  notificationCaregiverDTO: CaregiverNotification;
}
