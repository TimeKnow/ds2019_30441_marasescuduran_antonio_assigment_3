export class Caregiver {
  id: number;
  email: string;
  name: string;
  birthDate: string;
  gender: string;
  address: string;
}
