export class Patient {
  id: number;
  email: string;
  name: string;
  birthDate: string;
  gender: string;
  address: string;
  medicalRecord: string;
}
