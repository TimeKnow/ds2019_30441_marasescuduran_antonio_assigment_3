import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {Observable} from 'rxjs';
import {AuthUser} from '../../../core/models/auth-user';
import {selectCurrentAuthUser} from '../../../store/selectors/auth.selectors';
import {GetCurrentUser} from '../../../store/actions/auth.actions';

@Component({
  selector: 'app-menu-handler',
  templateUrl: './menu-handler.component.html',
  styleUrls: []
})
export class MenuHandlerComponent implements OnInit {
  currentUser$: Observable<AuthUser>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.store.dispatch(new GetCurrentUser(true));
    this.currentUser$ = this.store.select(selectCurrentAuthUser);
  }
}
