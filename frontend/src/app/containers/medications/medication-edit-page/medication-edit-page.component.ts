import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {ActivatedRoute} from '@angular/router';
import {
  selectMedicationStateIsLoading,
  selectMedicationStateSelectedMedication
} from '../../../store/selectors/medication.selectors';
import {Medication} from '../../../core/models/medication';
import {GetMedicationById, UpdateMedication} from '../../../store/actions/medication.actions';

@Component({
  selector: 'app-medication-edit-page',
  templateUrl: './medication-edit-page.component.html',
  styleUrls: ['./medication-edit-page.component.css']
})
export class MedicationEditPageComponent implements OnInit {

  currentId: number;
  isLoading$: Observable<boolean>;
  selectMedication$: Observable<Medication>;
  form: FormGroup;

  constructor(private store: Store<AppState>, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.currentId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);
    this.store.dispatch(new GetMedicationById(this.currentId));
    this.isLoading$ = this.store.select(selectMedicationStateIsLoading);
    this.selectMedication$ = this.store.select(selectMedicationStateSelectedMedication);
    this.form = new FormGroup({
      name: new FormControl(''),
      sideEffects: new FormControl(''),
      dosage: new FormControl(''),
    });
    this.selectMedication$.subscribe(medication => this.mapMedicationToFormContent(medication));
  }

  mapMedicationToFormContent(medication: Medication) {
    if (!medication) {
      return;
    }
    this.form.get('name').setValue(medication.name);
    this.form.get('sideEffects').setValue(medication.sideEffects);
    this.form.get('dosage').setValue(medication.dosage);
  }

  mapFormContentToMedication(): Medication {
    const medication = new Medication();
    medication.name = this.form.get('name').value;
    medication.sideEffects = this.form.get('sideEffects').value;
    medication.dosage = this.form.get('dosage').value;
    return medication;
  }

  onSave() {
    if (this.form.valid) {
      this.store.dispatch(new UpdateMedication(this.currentId, this.mapFormContentToMedication()));
    }
  }

}
