import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {Medication} from '../../../core/models/medication';
import {CreateMedication} from '../../../store/actions/medication.actions';

@Component({
  selector: 'app-medication-create-page',
  templateUrl: './medication-create-page.component.html',
  styleUrls: ['./medication-create-page.component.css']
})
export class MedicationCreatePageComponent implements OnInit {
  form: FormGroup;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(''),
      sideEffects: new FormControl(''),
      dosage: new FormControl(''),
    });
  }


  mapFormContentToMedication(): Medication {
    const medication = new Medication();
    medication.name = this.form.get('name').value;
    medication.sideEffects = this.form.get('sideEffects').value;
    medication.dosage = this.form.get('dosage').value;
    return medication;
  }

  onCreate() {
    if (this.form.valid) {
      const newMedication = this.mapFormContentToMedication();
      this.store.dispatch(new CreateMedication(newMedication));
    }

  }
}
