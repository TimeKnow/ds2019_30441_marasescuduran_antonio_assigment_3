import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {DeletePatient, GetAllPatients} from '../../../store/actions/patient.actions';
import {selectPatientIsLoading, selectPatientList} from '../../../store/selectors/patient.selectors';
import {Medication} from '../../../core/models/medication';
import {selectMedicationList, selectMedicationStateIsLoading} from '../../../store/selectors/medication.selectors';
import {DeleteMedication, GetAllMedications} from '../../../store/actions/medication.actions';

@Component({
  selector: 'app-medication-page',
  templateUrl: './medication-page.component.html',
  styleUrls: ['./medication-page.component.css']
})
export class MedicationPageComponent implements OnInit {

  medicationList$: Observable<Medication[]>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(new GetAllMedications());
    this.isLoading$ = this.store.select(selectMedicationStateIsLoading);
    this.medicationList$ = this.store.select(selectMedicationList);
  }

  onMedicationDelete(id: number) {
    this.store.dispatch(new DeleteMedication(id));
  }

}
