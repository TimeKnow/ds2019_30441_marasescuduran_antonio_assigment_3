import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {MedicationPlan} from '../../../core/models/medication-plan';
import {CreateMedicationPlan} from '../../../store/actions/medication-plan.actions';
import {Observable} from 'rxjs';
import {Medication} from '../../../core/models/medication';
import {Patient} from '../../../core/models/patient';
import {selectMedicationList} from '../../../store/selectors/medication.selectors';
import {selectPatientList} from '../../../store/selectors/patient.selectors';
import {GetAllMedications} from '../../../store/actions/medication.actions';
import {GetAllPatients} from '../../../store/actions/patient.actions';
import {GetCurrentUser} from '../../../store/actions/auth.actions';
import {AuthUser} from '../../../core/models/auth-user';
import {selectCurrentAuthUser} from '../../../store/selectors/auth.selectors';

@Component({
  selector: 'app-medication-plan-create-page',
  templateUrl: './medication-plan-create-page.component.html',
  styleUrls: ['./medication-plan-create-page.component.css']
})
export class MedicationPlanCreatePageComponent implements OnInit {
  form: FormGroup;
  currentUser$: Observable<AuthUser>;
  medications$: Observable<Medication[]>;
  patients$: Observable<Patient[]>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(new GetCurrentUser(true));
    this.store.dispatch(new GetAllMedications());
    this.store.dispatch(new GetAllPatients());
    this.currentUser$ = this.store.select(selectCurrentAuthUser);
    this.medications$ = this.store.select(selectMedicationList);
    this.patients$ = this.store.select(selectPatientList);
    this.currentUser$.subscribe(x => {
      this.form = new FormGroup({
        doctorId: new FormControl('',
          [Validators.required, Validators.pattern('^([1-9])$|^([1-9][0-9]+)$')]),
        patientId: new FormControl('',
          [Validators.required, Validators.pattern('^([1-9])$|^([1-9][0-9]+)$')]),
        intakeIntervals: new FormControl(''),
        period: new FormControl(''),
        medicationListIds: new FormControl('', [Validators.required]),
      });
      this.form.get('doctorId').setValue(x.id);
    });

  }

  mapFormContentToMedicationPlan(): MedicationPlan {
    const medicationPlan = new MedicationPlan();
    medicationPlan.doctorId = parseInt(this.form.get('doctorId').value, 10);
    medicationPlan.patientId = parseInt(this.form.get('patientId').value, 10);
    medicationPlan.intakeIntervals = this.form.get('intakeIntervals').value;
    medicationPlan.period = this.form.get('period').value;
    medicationPlan.medicationListIds = this.form.get('medicationListIds').value;
    return medicationPlan;
  }

  onCreate() {
    console.log(this.form);
    if (this.form.valid) {
      const newMedicationPlan = this.mapFormContentToMedicationPlan();
      this.store.dispatch(new CreateMedicationPlan(newMedicationPlan));
    }
  }

}
