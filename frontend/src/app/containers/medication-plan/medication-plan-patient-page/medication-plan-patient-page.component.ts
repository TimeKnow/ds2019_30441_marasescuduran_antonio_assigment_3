import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {MedicationPlan} from '../../../core/models/medication-plan';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {GetAllMedicationPlans, GetAllMedicationPlansByPatient} from '../../../store/actions/medication-plan.actions';
import {selectMedicationPlanList, selectMedicationPlanStateIsLoading} from '../../../store/selectors/medication-plan.selectors';
import {AuthUser} from '../../../core/models/auth-user';
import {selectCurrentAuthUser} from '../../../store/selectors/auth.selectors';
import {GetCurrentUser} from '../../../store/actions/auth.actions';

@Component({
  selector: 'app-medication-plan-patient-page',
  templateUrl: './medication-plan-patient-page.component.html',
  styleUrls: ['./medication-plan-patient-page.component.css']
})
export class MedicationPlanPatientPageComponent implements OnInit {

  medicationPlanList$: Observable<MedicationPlan[]>;
  isLoading$: Observable<boolean>;
  currentUser$: Observable<AuthUser>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(new GetCurrentUser(true));
    this.currentUser$ = this.store.select(selectCurrentAuthUser);
    this.currentUser$.subscribe(x => this.store.dispatch(new GetAllMedicationPlansByPatient(x.id)));
    this.medicationPlanList$ = this.store.select(selectMedicationPlanList);
    this.isLoading$ = this.store.select(selectMedicationPlanStateIsLoading);
  }

}
