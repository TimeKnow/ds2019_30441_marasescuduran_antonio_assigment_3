import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {Observable} from 'rxjs';
import {MedicationPlan} from '../../../core/models/medication-plan';
import {GetAllMedications} from '../../../store/actions/medication.actions';
import {selectMedicationPlanList, selectMedicationPlanStateIsLoading} from '../../../store/selectors/medication-plan.selectors';
import {GetAllMedicationPlans} from '../../../store/actions/medication-plan.actions';

@Component({
  selector: 'app-medication-plan-doctor-page',
  templateUrl: './medication-plan-doctor-page.component.html',
  styleUrls: ['./medication-plan-doctor-page.component.css']
})
export class MedicationPlanDoctorPageComponent implements OnInit {

  medicationPlanList$: Observable<MedicationPlan[]>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(new GetAllMedicationPlans());
    this.medicationPlanList$ = this.store.select(selectMedicationPlanList);
    this.isLoading$ = this.store.select(selectMedicationPlanStateIsLoading);
  }

}
