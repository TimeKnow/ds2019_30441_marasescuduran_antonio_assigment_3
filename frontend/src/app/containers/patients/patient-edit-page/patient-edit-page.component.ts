import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Patient} from '../../../core/models/patient';
import {AppState} from '../../../store/state/app.state';
import {Store} from '@ngrx/store';
import {selectPatientIsLoading, selectPatientStateSelectedPatient} from '../../../store/selectors/patient.selectors';
import {ActivatedRoute} from '@angular/router';
import {GetPatientById, UpdatePatient} from '../../../store/actions/patient.actions';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-patient-edit-page',
  templateUrl: './patient-edit-page.component.html',
  styleUrls: ['./patient-edit-page.component.css']
})
export class PatientEditPageComponent implements OnInit {
  currentId: number;
  isLoading$: Observable<boolean>;
  selectedPatient$: Observable<Patient>;
  form: FormGroup;

  constructor(private store: Store<AppState>, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.currentId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);
    this.store.dispatch(new GetPatientById(this.currentId));
    this.isLoading$ = this.store.select(selectPatientIsLoading);
    this.selectedPatient$ = this.store.select(selectPatientStateSelectedPatient);
    this.form = new FormGroup({
      email: new FormControl(''),
      name: new FormControl(''),
      birthDate: new FormControl(''),
      gender: new FormControl(''),
      address: new FormControl(''),
      medicalRecord: new FormControl(''),
    });
    this.selectedPatient$.subscribe(patient => this.setPatientFormContent(patient));
  }

  setPatientFormContent(patient: Patient) {
    if (!patient) {
      return;
    }
    const dateFormatted = patient.birthDate.split(' ').length > 1 ? patient.birthDate : patient.birthDate + ', 00:00:00';
    this.form.get('email').setValue(patient.email);
    this.form.get('name').setValue(patient.name);
    this.form.get('birthDate').setValue(new Date(dateFormatted));
    this.form.get('gender').setValue(patient.gender);
    this.form.get('address').setValue(patient.address);
    this.form.get('medicalRecord').setValue(patient.medicalRecord);
  }

  getPatientFromFormContent(): Patient {
    const patient = new Patient();
    patient.email = this.form.get('email').value;
    patient.name = this.form.get('name').value;
    patient.birthDate = this.form.get('birthDate').value.toLocaleString();
    patient.gender = this.form.get('gender').value;
    patient.address = this.form.get('address').value;
    patient.medicalRecord = this.form.get('medicalRecord').value;
    return patient;
  }

  onSave() {
    if (this.form.valid) {
      this.store.dispatch(new UpdatePatient(this.currentId, this.getPatientFromFormContent()));
    }
  }
}
