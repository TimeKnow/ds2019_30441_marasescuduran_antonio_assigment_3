import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {AppState} from '../../../store/state/app.state';
import {Store} from '@ngrx/store';
import {selectAuthErrorMessage} from '../../../store/selectors/auth.selectors';
import {LoginUser} from '../../../store/actions/auth.actions';
import {EmailCredential} from '../../../core/models/email-credential';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  errorMessage$: Observable<string>;
  loginForm = this.formBuilder.group({
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required
    ])
  });

  constructor(private formBuilder: FormBuilder, private store: Store<AppState>) {
  }

  ngOnInit() {
    this.errorMessage$ = this.store.select(selectAuthErrorMessage);
  }

  submitLogin() {
    const credential = new EmailCredential();
    credential.email = this.loginForm.get('email').value;
    credential.password = this.loginForm.get('password').value;
    this.store.dispatch(new LoginUser(credential));
  }

}
