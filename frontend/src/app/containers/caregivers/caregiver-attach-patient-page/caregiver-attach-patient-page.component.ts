import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {ActivatedRoute} from '@angular/router';
import {AttachPatientToCaregiver} from '../../../store/actions/caregiver.actions';
import {Observable} from 'rxjs';
import {Patient} from '../../../core/models/patient';
import {selectPatientList} from '../../../store/selectors/patient.selectors';
import {GetAllPatients} from '../../../store/actions/patient.actions';

@Component({
  selector: 'app-caregiver-attach-patient-page',
  templateUrl: './caregiver-attach-patient-page.component.html',
  styleUrls: ['./caregiver-attach-patient-page.component.css']
})
export class CaregiverAttachPatientPageComponent implements OnInit {

  form: FormGroup;
  patients$: Observable<Patient[]>;
  currentId: number;

  constructor(private store: Store<AppState>, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.currentId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);
    this.store.dispatch(new GetAllPatients());
    this.patients$ = this.store.select(selectPatientList);
    this.form = new FormGroup({
      patientId: new FormControl('',
        [Validators.required, Validators.pattern('^([1-9])$|^([1-9][0-9]+)$')]),
    });
  }

  onAttachPatient() {
    if (this.form.valid) {
      const patientId = parseInt(this.form.get('patientId').value, 10);
      this.store.dispatch(new AttachPatientToCaregiver(this.currentId, patientId));
    }
  }
}
