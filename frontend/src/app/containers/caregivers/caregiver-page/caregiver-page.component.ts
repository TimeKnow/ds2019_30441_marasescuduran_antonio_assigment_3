import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Caregiver} from '../../../core/models/caregiver';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {DeleteCaregiver, GetAllCaregivers} from '../../../store/actions/caregiver.actions';
import {selectCaregiverList, selectCaregiverStateIsLoading} from '../../../store/selectors/caregiver.selectors';;

@Component({
  selector: 'app-caregiver-page',
  templateUrl: './caregiver-page.component.html',
  styleUrls: ['./caregiver-page.component.css']
})
export class CaregiverPageComponent implements OnInit {

  caregiverList$: Observable<Caregiver[]>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.store.dispatch(new GetAllCaregivers());
    this.isLoading$ = this.store.select(selectCaregiverStateIsLoading);
    this.caregiverList$ = this.store.select(selectCaregiverList);
  }

  onCaregiverDelete(id: number) {
    this.store.dispatch(new DeleteCaregiver(id));
  }
}
