import {Component, OnDestroy, OnInit} from '@angular/core';
import {RxStompService} from '@stomp/ng2-stompjs';
import {Message} from '@stomp/stompjs';
import {NotificationEvent} from '../../../core/models/notification-event';
import CaregiverNotification from '../../../core/models/caregiver-notification';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-patient-notifications-page',
  templateUrl: './patient-notifications-page.component.html',
  styleUrls: ['./patient-notifications-page.component.css']
})
export class PatientNotificationsPageComponent implements OnInit, OnDestroy {

  notifications: CaregiverNotification[] = [];
  topicSubscription: Subscription;

  constructor(private rxStompService: RxStompService) {
  }

  ngOnInit() {
    this.topicSubscription = this.rxStompService.watch('/topic/notifications')
      .subscribe((message: Message) => {
        const notificationEvent: NotificationEvent = JSON.parse(message.body);
        if (notificationEvent.type === 'NOTIFICATION_CREATED') {
          this.notifications = this.notifications.concat([notificationEvent.notificationCaregiverDTO]);
        }
      });
  }

  ngOnDestroy() {
    this.topicSubscription.unsubscribe();
  }

}
