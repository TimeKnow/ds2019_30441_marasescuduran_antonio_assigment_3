import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/state/app.state';
import {Caregiver} from '../../../core/models/caregiver';
import {CreateCaregiver} from '../../../store/actions/caregiver.actions';

@Component({
  selector: 'app-caregiver-create-page',
  templateUrl: './caregiver-create-page.component.html',
  styleUrls: ['./caregiver-create-page.component.css']
})
export class CaregiverCreatePageComponent implements OnInit {
  form: FormGroup;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(''),
      name: new FormControl(''),
      birthDate: new FormControl(''),
      gender: new FormControl(''),
      address: new FormControl(''),
    });
  }

  onCreate() {
    if (this.form.valid) {
      const newCaregiver = new Caregiver();
      newCaregiver.email = this.form.get('email').value;
      newCaregiver.name = this.form.get('name').value;
      newCaregiver.birthDate = this.form.get('birthDate').value.toLocaleString();
      newCaregiver.gender = this.form.get('gender').value;
      newCaregiver.address = this.form.get('address').value;
      this.store.dispatch(new CreateCaregiver(newCaregiver));
    }
  }
}
