import {AppState} from '../state/app.state';
import {createSelector} from '@ngrx/store';
import {PatientState} from '../state/patient.state';

const selectPatientState = (state: AppState) => state.patientState;

export const selectPatientList = createSelector(
  selectPatientState,
  (state: PatientState) => state.patientList
);

export const selectPatientStateSelectedPatient = createSelector(
  selectPatientState,
  (state: PatientState) => state.selectedPatient
);

export const selectPatientIsLoading = createSelector(
  selectPatientState,
  (state: PatientState) => state.isLoading
);

export const selectPatientErrorMessage = createSelector(
  selectPatientState,
  (state: PatientState) => state.isLoading
);
