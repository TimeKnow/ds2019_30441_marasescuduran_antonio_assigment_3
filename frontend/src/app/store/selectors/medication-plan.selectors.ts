import {AppState} from '../state/app.state';
import {createSelector} from '@ngrx/store';
import {MedicationPlanState} from '../state/medication-plan.state';

const selectMedicationPlanState = (state: AppState) => state.medicationPlanState;

export const selectMedicationPlanList = createSelector(
  selectMedicationPlanState,
  (state: MedicationPlanState) => state.medicationPlanList
);


export const selectMedicationPlanStateIsLoading = createSelector(
  selectMedicationPlanState,
  (state: MedicationPlanState) => state.isLoading
);

export const selectMedicationPlanStateErrorMessage = createSelector(
  selectMedicationPlanState,
  (state: MedicationPlanState) => state.errorMessage
);
