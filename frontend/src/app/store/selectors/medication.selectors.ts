import {AppState} from '../state/app.state';
import {createSelector} from '@ngrx/store';
import {CaregiverState} from '../state/caregiver.state';
import {MedicationState} from '../state/medication.state';

const selectMedicationState = (state: AppState) => state.medicationState;

export const selectMedicationList = createSelector(
  selectMedicationState,
  (state: MedicationState) => state.medicationList
);

export const selectMedicationStateSelectedMedication = createSelector(
  selectMedicationState,
  (state: MedicationState) => state.selectedMedication
);

export const selectMedicationStateIsLoading = createSelector(
  selectMedicationState,
  (state: MedicationState) => state.isLoading
);

export const selectMedicationStateErrorMessage = createSelector(
  selectMedicationState,
  (state: MedicationState) => state.errorMessage
);
