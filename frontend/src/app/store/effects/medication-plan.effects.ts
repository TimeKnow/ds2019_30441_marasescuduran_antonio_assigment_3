import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap, tap, timeout} from 'rxjs/operators';
import {of} from 'rxjs';
import {Router} from '@angular/router';
import {MedicationPlanService} from '../../core/services/medication-plan.service';
import {
  CreateMedicationPlan, CreateMedicationPlanFailure, CreateMedicationPlanSuccess,
  GetAllMedicationPlans, GetAllMedicationPlansByPatient, GetAllMedicationPlansByPatientFailure, GetAllMedicationPlansByPatientSuccess,
  GetAllMedicationPlansFailure,
  GetAllMedicationPlansSuccess,
  MedicationPlanActionsTypes
} from '../actions/medication-plan.actions';

@Injectable()
export class MedicationPlanEffects {
  constructor(
    private medicationPlanService: MedicationPlanService,
    private actions$: Actions,
    private router: Router,
  ) {
  }

  @Effect()
  getAllMedicationPlans$ = this.actions$.pipe(
    ofType<GetAllMedicationPlans>(MedicationPlanActionsTypes.GetAllMedicationPlans),
    switchMap(credential => this.medicationPlanService.getAllMedicationPlans().pipe(
      timeout(5000),
      map(medicationPlans => new GetAllMedicationPlansSuccess(medicationPlans)),
      catchError(error => of(new GetAllMedicationPlansFailure(error)))
    ))
  );

  @Effect()
  getAllMedicationByPatientPlans$ = this.actions$.pipe(
    ofType<GetAllMedicationPlansByPatient>(MedicationPlanActionsTypes.GetAllMedicationPlansByPatient),
    map(action => action.payload),
    switchMap(id => this.medicationPlanService.getAllMedicationPlansForPatient(id).pipe(
      timeout(5000),
      map(medicationPlans => new GetAllMedicationPlansByPatientSuccess(medicationPlans)),
      catchError(error => of(new GetAllMedicationPlansByPatientFailure(error)))
    ))
  );

  @Effect()
  createMedicationPlan$ = this.actions$.pipe(
    ofType<CreateMedicationPlan>(MedicationPlanActionsTypes.CreateMedicationPlan),
    map(action => action.payload),
    switchMap(newMedicationPlan => this.medicationPlanService.createMedicationPlan(newMedicationPlan).pipe(
      timeout(3000),
      map(createdMedication => new CreateMedicationPlanSuccess(createdMedication)),
      catchError(error => of(new CreateMedicationPlanFailure(error)))
    ))
  );

  @Effect({dispatch: false})
  createMedicationSuccess$ = this.actions$.pipe(
    ofType<CreateMedicationPlanSuccess>(MedicationPlanActionsTypes.CreateMedicationPlanSuccess),
    tap(() => this.router.navigateByUrl('/medications/plans/doctor'))
  );
}
