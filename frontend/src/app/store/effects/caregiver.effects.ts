import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap, tap, timeout} from 'rxjs/operators';
import {of} from 'rxjs';
import {CaregiverService} from '../../core/services/caregiver.service';
import {
  AttachPatientToCaregiver, AttachPatientToCaregiverFailure, AttachPatientToCaregiverSuccess,
  CaregiverActionsTypes,
  CreateCaregiver,
  CreateCaregiverFailure,
  CreateCaregiverSuccess,
  DeleteCaregiver,
  DeleteCaregiverFailure,
  DeleteCaregiverSuccess,
  GetAllCaregivers,
  GetAllCaregiversFailure,
  GetAllCaregiversSuccess, GetAllPatientsOfCaregivers, GetAllPatientsOfCaregiversFailure, GetAllPatientsOfCaregiversSuccess,
  GetCaregiverById,
  GetCaregiverByIdFailure,
  GetCaregiverByIdSuccess,
  UpdateCaregiver,
  UpdateCaregiverFailure,
  UpdateCaregiverSuccess
} from '../actions/caregiver.actions';
import {Router} from '@angular/router';

@Injectable()
export class CaregiverEffects {
  constructor(
    private caregiverService: CaregiverService,
    private actions$: Actions,
    private router: Router
  ) {
  }

  @Effect()
  getAllCaregivers$ = this.actions$.pipe(
    ofType<GetAllCaregivers>(CaregiverActionsTypes.GetAllCaregivers),
    switchMap(credential => this.caregiverService.getAllCaregivers().pipe(
      timeout(3000),
      map(caregivers => new GetAllCaregiversSuccess(caregivers)),
      catchError(error => of(new GetAllCaregiversFailure(error)))
    ))
  );

  @Effect()
  deleteCaregiver$ = this.actions$.pipe(
    ofType<DeleteCaregiver>(CaregiverActionsTypes.DeleteCaregiver),
    map(action => action.payload),
    switchMap(id => this.caregiverService.deleteCaregiver(id).pipe(
      timeout(5000),
      map(() => new DeleteCaregiverSuccess()),
      catchError(error => of(new DeleteCaregiverFailure(error)))
    ))
  );

  @Effect()
  getCaregiverById$ = this.actions$.pipe(
    ofType<GetCaregiverById>(CaregiverActionsTypes.GetCaregiverById),
    map(action => action.payload),
    switchMap(caregiverId => this.caregiverService.getCaregiverById(caregiverId).pipe(
      timeout(5000),
      map(caregiver => new GetCaregiverByIdSuccess(caregiver)),
      catchError(error => of(new GetCaregiverByIdFailure(error)))
    ))
  );

  @Effect()
  updateCaregiver$ = this.actions$.pipe(
    ofType<UpdateCaregiver>(CaregiverActionsTypes.UpdateCaregiver),
    switchMap(action => this.caregiverService.updateCaregiver(action.id, action.payload).pipe(
      timeout(3000),
      map(updatedCaregiver => new UpdateCaregiverSuccess(action.id, updatedCaregiver)),
      catchError(error => of(new UpdateCaregiverFailure(error)))
    ))
  );

  @Effect({dispatch: false})
  updateCaregiverSuccess$ = this.actions$.pipe(
    ofType<UpdateCaregiverSuccess>(CaregiverActionsTypes.UpdateCaregiverSuccess),
    tap(() => this.router.navigateByUrl('/caregivers'))
  );

  @Effect()
  createCaregiver$ = this.actions$.pipe(
    ofType<CreateCaregiver>(CaregiverActionsTypes.CreateCaregiver),
    map(action => action.payload),
    switchMap(newCaregiver => this.caregiverService.createCaregiver(newCaregiver).pipe(
      timeout(3000),
      map(createdCaregiver => new CreateCaregiverSuccess(createdCaregiver)),
      catchError(error => of(new CreateCaregiverFailure(error)))
    ))
  );

  @Effect({dispatch: false})
  createCaregiverSuccess$ = this.actions$.pipe(
    ofType<CreateCaregiverSuccess>(CaregiverActionsTypes.CreateCaregiverSuccess),
    tap(() => this.router.navigateByUrl('/caregivers'))
  );

  @Effect()
  attachPatientToCaregiver$ = this.actions$.pipe(
    ofType<AttachPatientToCaregiver>(CaregiverActionsTypes.AttachPatientToCaregiver),
    switchMap(action => this.caregiverService.attachPatientToCaregiver(action.caregiverId, action.patientId).pipe(
      map(attachedPatient => new AttachPatientToCaregiverSuccess(attachedPatient)),
      catchError(error => of(new AttachPatientToCaregiverFailure(error)))
    ))
  );

  @Effect({dispatch: false})
  attachPatientToCaregiverSuccess$ = this.actions$.pipe(
    ofType<AttachPatientToCaregiverSuccess>(CaregiverActionsTypes.AttachPatientToCaregiverSuccess),
    tap(() => this.router.navigateByUrl('/caregivers'))
  );

  @Effect()
  getAllPatientsOfCaregivers$ = this.actions$.pipe(
    ofType<GetAllPatientsOfCaregivers>(CaregiverActionsTypes.GetAllPatientsOfCaregivers),
    switchMap(action => this.caregiverService.getPatientsOfCaregiver(action.caregiverId).pipe(
      map(caregiverPatients => new GetAllPatientsOfCaregiversSuccess(caregiverPatients)),
      catchError(error => of(new GetAllPatientsOfCaregiversFailure(error)))
    ))
  );
}
