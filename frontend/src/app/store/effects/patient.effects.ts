import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap, tap, timeout} from 'rxjs/operators';
import {of} from 'rxjs';
import {
  CreatePatient, CreatePatientFailure, CreatePatientSuccess,
  DeletePatient, DeletePatientFailure,
  DeletePatientSuccess,
  GetAllPatients,
  GetAllPatientsFailure,
  GetAllPatientsSuccess, GetPatientById, GetPatientByIdFailure, GetPatientByIdSuccess,
  PatientActionsTypes, UpdatePatient, UpdatePatientFailure, UpdatePatientSuccess
} from '../actions/patient.actions';
import {PatientService} from '../../core/services/patient.service';
import {Router} from '@angular/router';

@Injectable()
export class PatientEffects {
  constructor(
    private patientService: PatientService,
    private actions$: Actions,
    private router: Router
  ) {
  }

  @Effect()
  getAllPatients$ = this.actions$.pipe(
    ofType<GetAllPatients>(PatientActionsTypes.GetAllPatients),
    switchMap(() => this.patientService.getAllPatients().pipe(
      timeout(5000),
      map(patients => new GetAllPatientsSuccess(patients)),
      catchError(error => of(new GetAllPatientsFailure(error)))
    ))
  );

  @Effect()
  getPatientById$ = this.actions$.pipe(
    ofType<GetPatientById>(PatientActionsTypes.GetPatientById),
    map(action => action.payload),
    switchMap(patientId => this.patientService.getPatientById(patientId).pipe(
      timeout(5000),
      map(patient => new GetPatientByIdSuccess(patient)),
      catchError(error => of(new GetPatientByIdFailure(error)))
    ))
  );

  @Effect()
  deletePatient$ = this.actions$.pipe(
    ofType<DeletePatient>(PatientActionsTypes.DeletePatient),
    map(action => action.payload),
    switchMap(id => this.patientService.deletePatient(id).pipe(
      timeout(3000),
      map(() => new DeletePatientSuccess()),
      catchError(error => of(new DeletePatientFailure(error)))
    ))
  );

  @Effect()
  updatePatient$ = this.actions$.pipe(
    ofType<UpdatePatient>(PatientActionsTypes.UpdatePatient),
    switchMap(action => this.patientService.updatePatient(action.id, action.payload).pipe(
      timeout(3000),
      map(updatedPatient => new UpdatePatientSuccess(action.id, updatedPatient)),
      catchError(error => of(new UpdatePatientFailure(error)))
    ))
  );

  @Effect({dispatch: false})
  updatePatientSuccess$ = this.actions$.pipe(
    ofType<UpdatePatientSuccess>(PatientActionsTypes.UpdatePatientSuccess),
    tap(() => this.router.navigateByUrl('/patients'))
  );

  @Effect()
  createPatient$ = this.actions$.pipe(
    ofType<CreatePatient>(PatientActionsTypes.CreatePatient),
    map(action => action.payload),
    switchMap(newPatient => this.patientService.createPatient(newPatient).pipe(
      timeout(3000),
      map(createdPatient => new CreatePatientSuccess(createdPatient)),
      catchError(error => of(new CreatePatientFailure(error)))
    ))
  );

  @Effect({dispatch: false})
  createPatientSuccess$ = this.actions$.pipe(
    ofType<CreatePatientSuccess>(PatientActionsTypes.CreatePatientSuccess),
    tap(() => this.router.navigateByUrl('/patients'))
  );
}
