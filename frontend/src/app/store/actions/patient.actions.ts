import {Action} from '@ngrx/store';
import {Patient} from '../../core/models/patient';

export enum PatientActionsTypes {
  GetAllPatients = '[Patient] Get All',
  GetAllPatientsSuccess = '[Patient] Get All Success',
  GetAllPatientsFailure = '[Patient] Get All Failure',
  GetPatientById = '[Patient] Get By Id',
  GetPatientByIdSuccess = '[Patient] Get By Id Success',
  GetPatientByIdFailure = '[Patient] Get By Id Failure',
  DeletePatient = '[Patient] Delete',
  DeletePatientSuccess = '[Patient] Delete Success',
  DeletePatientFailure = '[Patient] Delete Failure',
  UpdatePatient = '[Patient] Update',
  UpdatePatientSuccess = '[Patient] Update Success',
  UpdatePatientFailure = '[Patient] Update Failure',
  CreatePatient = '[Patient] Create',
  CreatePatientSuccess = '[Patient] Create Success',
  CreatePatientFailure = '[Patient] Create Failure',
}

export class GetAllPatients implements Action {
  public readonly type = PatientActionsTypes.GetAllPatients;

  constructor() {
  }
}

export class GetAllPatientsSuccess implements Action {
  public readonly type = PatientActionsTypes.GetAllPatientsSuccess;

  constructor(public payload: Patient[]) {
  }
}

export class GetAllPatientsFailure implements Action {
  public readonly type = PatientActionsTypes.GetAllPatientsFailure;

  constructor(public payload: any) {
  }
}

export class GetPatientById implements Action {
  public readonly type = PatientActionsTypes.GetPatientById;

  constructor(public payload: number) {
  }
}

export class GetPatientByIdSuccess implements Action {
  public readonly type = PatientActionsTypes.GetPatientByIdSuccess;

  constructor(public payload: Patient) {
  }
}

export class GetPatientByIdFailure implements Action {
  public readonly type = PatientActionsTypes.GetPatientByIdFailure;

  constructor(public payload: any) {
  }
}

export class DeletePatient implements Action {
  public readonly type = PatientActionsTypes.DeletePatient;

  constructor(public payload: number) {
  }
}

export class DeletePatientSuccess implements Action {
  public readonly type = PatientActionsTypes.DeletePatientSuccess;

}

export class DeletePatientFailure implements Action {
  public readonly type = PatientActionsTypes.DeletePatientFailure;

  constructor(public payload: any) {
  }
}

export class UpdatePatient implements Action {
  public readonly type = PatientActionsTypes.UpdatePatient;

  constructor(public id: number, public payload: Patient) {
  }
}

export class UpdatePatientSuccess implements Action {
  public readonly type = PatientActionsTypes.UpdatePatientSuccess;

  constructor(public id: number, public payload: Patient) {
  }

}

export class UpdatePatientFailure implements Action {
  public readonly type = PatientActionsTypes.UpdatePatientFailure;

  constructor(public payload: any) {
  }
}

export class CreatePatient implements Action {
  public readonly type = PatientActionsTypes.CreatePatient;

  constructor(public payload: Patient) {
  }
}

export class CreatePatientSuccess implements Action {
  public readonly type = PatientActionsTypes.CreatePatientSuccess;

  constructor(public payload: Patient) {
  }
}

export class CreatePatientFailure implements Action {
  public readonly type = PatientActionsTypes.CreatePatientFailure;

  constructor(public payload: any) {
  }
}

export type PatientActions =
  GetAllPatients
  | GetAllPatientsSuccess
  | GetAllPatientsFailure
  | GetPatientById
  | GetPatientByIdSuccess
  | GetPatientByIdFailure
  | DeletePatient
  | DeletePatientSuccess
  | DeletePatientFailure
  | UpdatePatient
  | UpdatePatientSuccess
  | UpdatePatientFailure
  | CreatePatient
  | CreatePatientSuccess
  | CreatePatientFailure;
