import {AuthState, initialAuthState} from '../state/auth.state';
import {AuthActions, AuthActionsTypes} from '../actions/auth.actions';

export function authReducers(state: AuthState = initialAuthState, action: AuthActions) {
  switch (action.type) {
    case AuthActionsTypes.LoginUser:
      return {
        ...state,
        loadingLogin: true,
      };
    case AuthActionsTypes.LoginUserSuccess:
      return {
        ...state,
        loadingLogin: false,
        errorMessage: null
      };
    case AuthActionsTypes.LoginUserFailure:
      return {
        ...state,
        loadingLogin: false,
        errorMessage: action.payload
      };
    case AuthActionsTypes.GetCurrentUserSuccess:
      return {
        ...state,
        loadingCurrentUser: false,
        currentUser: action.payload
      };
    case AuthActionsTypes.LogoutUser:
      return {
        ...state,
        currentUser: null
      };
    default:
      return state;
  }
}
