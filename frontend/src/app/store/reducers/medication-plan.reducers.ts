import {initialMedicationPlanState, MedicationPlanState} from '../state/medication-plan.state';
import {MedicationPlanActions, MedicationPlanActionsTypes} from '../actions/medication-plan.actions';

export function medicationPlanReducers(state: MedicationPlanState = initialMedicationPlanState, action: MedicationPlanActions) {
  switch (action.type) {
    case MedicationPlanActionsTypes.GetAllMedicationPlans:
      return {
        ...state,
        isLoading: true,
      };
    case MedicationPlanActionsTypes.GetAllMedicationPlansSuccess:
      return {
        ...state,
        medicationPlanList: action.payload,
        isLoading: false,
      };
    case MedicationPlanActionsTypes.GetAllMedicationPlansFailure:
      return {
        ...state,
        errorMessage: action.payload,
        medicationPlanList: [],
        isLoading: false
      };
    case MedicationPlanActionsTypes.GetAllMedicationPlansByPatient:
      return {
        ...state,
        isLoading: true,
      };
    case MedicationPlanActionsTypes.GetAllMedicationPlansByPatientSuccess:
      return {
        ...state,
        medicationPlanList: action.payload,
        isLoading: false,
      };
    case MedicationPlanActionsTypes.GetAllMedicationPlansByPatientFailure:
      return {
        ...state,
        errorMessage: action.payload,
        medicationPlanList: [],
        isLoading: false
      };
    case MedicationPlanActionsTypes.CreateMedicationPlan:
      return {
        ...state,
        isLoading: true,
      };
    case MedicationPlanActionsTypes.CreateMedicationPlanSuccess:
      return {
        ...state,
        medicationPlanList: state.medicationPlanList.concat([action.payload]),
        isLoading: false,
      };
    case MedicationPlanActionsTypes.CreateMedicationPlanFailure:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false
      };
    default:
      return state;
  }
}
