import {PatientActions, PatientActionsTypes} from '../actions/patient.actions';
import {initialPatientState, PatientState} from '../state/patient.state';

export function patientReducers(state: PatientState = initialPatientState, action: PatientActions) {
  switch (action.type) {
    case PatientActionsTypes.GetAllPatients:
      return {
        ...state,
        isLoading: true,
      };
    case PatientActionsTypes.GetAllPatientsSuccess:
      return {
        ...state,
        isLoading: false,
        patientList: action.payload
      };
    case PatientActionsTypes.GetAllPatientsFailure:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload
      };
    case PatientActionsTypes.GetPatientById:
      return {
        ...state,
        isLoading: true
      };
    case PatientActionsTypes.GetPatientByIdSuccess:
      return {
        ...state,
        isLoading: false,
        selectedPatient: action.payload
      };
    case PatientActionsTypes.GetPatientByIdFailure:
      return {
        ...state,
        selectedPatient: null,
        isLoading: false,
        errorMessage: action.payload
      };
    case PatientActionsTypes.DeletePatient:
      return {
        ...state,
        isLoading: true,
        patientList: state.patientList.filter(x => x.id !== action.payload)
      };
    case PatientActionsTypes.DeletePatientSuccess:
      return {
        ...state,
        isLoading: false,
      };
    case PatientActionsTypes.DeletePatientFailure:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };
    case PatientActionsTypes.UpdatePatient:
      return {
        ...state,
        isLoading: true,
      };
    case PatientActionsTypes.UpdatePatientSuccess:
      const statePatientList = state.patientList;
      statePatientList[action.id] = action.payload;
      return {
        ...state,
        isLoading: false,
        patientList: statePatientList
      };
    case PatientActionsTypes.UpdatePatientFailure:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };
    case PatientActionsTypes.CreatePatient:
      return {
        ...state,
        isLoading: true,
      };
    case PatientActionsTypes.CreatePatientSuccess:
      return {
        ...state,
        isLoading: false,
        patientList: state.patientList.concat([action.payload])
      };
    case PatientActionsTypes.CreatePatientFailure:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };
    default:
      return state;
  }
}
