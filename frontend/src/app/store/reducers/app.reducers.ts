import {AppState} from '../state/app.state';
import {ActionReducerMap} from '@ngrx/store';
import {authReducers} from './auth.reducers';
import {patientReducers} from './patient.reducers';
import {caregiverReducers} from './caregiver.reducers';
import {medicationReducers} from './medication.reducers';
import {medicationPlanReducers} from './medication-plan.reducers';

export const appReducers: ActionReducerMap<AppState, any> = {
  authState: authReducers,
  patientState: patientReducers,
  caregiverState: caregiverReducers,
  medicationState: medicationReducers,
  medicationPlanState: medicationPlanReducers
};
