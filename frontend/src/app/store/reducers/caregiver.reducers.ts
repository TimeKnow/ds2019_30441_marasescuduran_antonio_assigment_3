import {CaregiverState, initialCaregiverState} from '../state/caregiver.state';
import {CaregiverActions, CaregiverActionsTypes} from '../actions/caregiver.actions';

export function caregiverReducers(state: CaregiverState = initialCaregiverState, action: CaregiverActions) {
  switch (action.type) {
    case CaregiverActionsTypes.GetAllCaregivers:
      return {
        ...state,
        isLoading: true,
      };
    case CaregiverActionsTypes.GetAllCaregiversSuccess:
      return {
        ...state,
        isLoading: false,
        caregiverList: action.payload
      };
    case CaregiverActionsTypes.GetAllCaregiversFailure:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload
      };
    case CaregiverActionsTypes.GetCaregiverById:
      return {
        ...state,
        isLoading: true
      };
    case CaregiverActionsTypes.GetCaregiverByIdSuccess:
      return {
        ...state,
        isLoading: false,
        selectedCaregiver: action.payload
      };
    case CaregiverActionsTypes.GetCaregiverByIdFailure:
      return {
        ...state,
        selectedCaregiver: null,
        isLoading: false,
        errorMessage: action.payload
      };
    case CaregiverActionsTypes.DeleteCaregiver:
      return {
        ...state,
        isLoading: true,
        caregiverList: state.caregiverList.filter(x => x.id !== action.payload)
      };
    case CaregiverActionsTypes.DeleteCaregiverSuccess:
      return {
        ...state,
        isLoading: false,
      };
    case CaregiverActionsTypes.DeleteCaregiverFailure:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };
    case CaregiverActionsTypes.UpdateCaregiver:
      return {
        ...state,
        isLoading: true,
      };
    case CaregiverActionsTypes.UpdateCaregiverSuccess:
      const stateCaregiverList = state.caregiverList;
      stateCaregiverList[action.id] = action.payload;
      return {
        ...state,
        isLoading: false,
        caregiverList: stateCaregiverList
      };
    case CaregiverActionsTypes.UpdateCaregiverFailure:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };
    case CaregiverActionsTypes.CreateCaregiver:
      return {
        ...state,
        isLoading: true,
      };
    case CaregiverActionsTypes.CreateCaregiverSuccess:
      return {
        ...state,
        isLoading: false,
        caregiverList: state.caregiverList.concat([action.payload])
      };
    case CaregiverActionsTypes.CreateCaregiverFailure:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false,
      };
    case CaregiverActionsTypes.GetAllPatientsOfCaregivers:
      return {
        ...state,
        isLoading: true,
      };
    case CaregiverActionsTypes.GetAllPatientsOfCaregiversSuccess:
      return {
        ...state,
        isLoading: false,
        caregiverPatientList: action.payload
      };
    case CaregiverActionsTypes.GetAllPatientsOfCaregiversFailure:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload
      };
    case CaregiverActionsTypes.AttachPatientToCaregiver:
      return {
        ...state,
        isLoading: true,
      };
    case CaregiverActionsTypes.AttachPatientToCaregiverSuccess:
      return {
        ...state,
        isLoading: false,
        caregiverPatientList: state.caregiverPatientList.concat([action.payload])
      };
    case CaregiverActionsTypes.AttachPatientToCaregiverFailure:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload
      };
    default:
      return state;
  }
}
