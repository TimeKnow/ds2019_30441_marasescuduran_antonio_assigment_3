import {AuthUser} from '../../core/models/auth-user';

export interface AuthState {
  loadingLogin: boolean;
  loadingCurrentUser: boolean;
  currentUser: AuthUser;
  errorMessage: string;
}

export const initialAuthState: AuthState = {
  loadingLogin: false,
  loadingCurrentUser: false,
  currentUser: null,
  errorMessage: null
};
