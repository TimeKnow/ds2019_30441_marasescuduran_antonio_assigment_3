import {MedicationPlan} from '../../core/models/medication-plan';

export interface MedicationPlanState {
  medicationPlanList: MedicationPlan[];
  isLoading: boolean;
  errorMessage: string | null;
}


export const initialMedicationPlanState: MedicationPlanState = {
  medicationPlanList: [],
  isLoading: false,
  errorMessage: null
};
