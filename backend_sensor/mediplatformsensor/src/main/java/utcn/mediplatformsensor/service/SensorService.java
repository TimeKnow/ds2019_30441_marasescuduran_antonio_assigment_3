package utcn.mediplatformsensor.service;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static utcn.mediplatformsensor.config.SpringConfig.SFG_MESSAGE_QUEUE;

@Service
@RequiredArgsConstructor
public class SensorService {
    private final RabbitTemplate rabbitTemplate;
    private static Integer line = 0;
    private static final String READING_ERROR_MESSAGE = "Reading Error";
    private static final Integer PATIENT_ID = 2;

    @Scheduled(fixedDelay = 1000)
    public void sendSensorData() {
        String data = readSensorData();
        System.out.println(data);
        rabbitTemplate.convertAndSend(SFG_MESSAGE_QUEUE, convertSensorData(data));
    }

    private HashMap<String, String> convertSensorData(String data) {
        HashMap<String, String> convertedData = new HashMap<>();
        val separatedData = data.split("\t\t");

        long start = LocalDateTime.parse(separatedData[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                .atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        long end = LocalDateTime.parse(separatedData[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                .atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        String activity = separatedData[2];

        convertedData.put("patient_id", PATIENT_ID.toString());
        convertedData.put("activity", activity);
        convertedData.put("start", Long.toString(start));
        convertedData.put("end", Long.toString(end));
        return convertedData;
    }


    private String readSensorData() {
        try {
            InputStream sensorData = new ClassPathResource("sensor-data.txt").getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(sensorData));
            List<String> lines = reader.lines().collect(Collectors.toList());
            SensorService.line = (SensorService.line.equals(lines.size())) ? 0 : SensorService.line + 1;
            return lines.get(SensorService.line);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return READING_ERROR_MESSAGE;
    }
}
