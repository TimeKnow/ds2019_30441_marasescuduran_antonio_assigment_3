package utcn.mediplatformsensor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MediplatformsensorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediplatformsensorApplication.class, args);
	}

}
