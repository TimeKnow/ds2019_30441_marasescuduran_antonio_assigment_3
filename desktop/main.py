from core.graphics.pillbox_app import PillboxApp
from core.services.pillbox_service import PillboxService
from datetime import datetime, timedelta


def get_predefined_time():
    current_time = datetime.now() + timedelta(seconds=3)
    return current_time.strftime("%H:%M:%S")


patient_id = 2
predefined_time = get_predefined_time()


def run():
    service = PillboxService(patient_id)
    print(predefined_time)
    app = PillboxApp(service, predefined_time)
    app.run()


if __name__ == "__main__":
    run()
