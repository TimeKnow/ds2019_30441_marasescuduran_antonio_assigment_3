from core.models.medication_plan_status import MedicationPlanStatusModel
from pillbox_pb2 import PatientRPC, MedicationPlanStatus
from pillbox_pb2_grpc import PillboxServiceStub
from google.protobuf.json_format import MessageToJson

import json
import grpc


class PillboxService:
    def __init__(self, patient_id):
        self.patient_id = patient_id
        self.channel = grpc.insecure_channel('localhost:6565')
        self.stub = PillboxServiceStub(self.channel)

    def get_medication_plans_for_patients(self):
        response = self.stub.getMedicationPlan(PatientRPC(id=self.patient_id))
        x = json.loads(MessageToJson(response))
        if 'medicationPlans' in x:
            return x['medicationPlans']
        else:
            return None

    def notify_medication_taken(self, data: MedicationPlanStatusModel):
        response = self.stub.notifyMedicationTaken(
            MedicationPlanStatus(medicationPlanId=data.medication_plan_id, medicationId=data.medication_id,
                                 taken=True,
                                 notificationDate=data.notification_date))
        return json.loads(MessageToJson(response))['message']

    def notify_medication_not_taken(self, data: MedicationPlanStatus):
        response = self.stub.notifyMedicationNotTaken(
            MedicationPlanStatus(medicationPlanId=data.medication_plan_id, medicationId=data.medication_id, taken=False,
                                 notificationDate=data.notification_date))
        return json.loads(MessageToJson(response))['message']

    def close(self):
        self.channel.close()
