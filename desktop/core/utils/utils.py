import functools
from datetime import datetime, timedelta


def flatmap_medications_from_plans(medication_plans):
    return medication_plans[0]['medications'] if len(medication_plans) == 1 else functools.reduce(
        (lambda x, y: x['medications'] + y['medications']), medication_plans, [])


def formatt_medication_plan_to_str(medication_plans):
    response = []
    for plan in medication_plans:
        response = formatt_medication_to_str(plan['medications'])
        response = map((lambda x: x + " Intake:" + plan['intakeIntervals']), response)
    return list(response)


def formatt_medication_to_str(medication_list):
    return map((lambda x: str(x['id']) + " " + str(x['name']) + " Dosage: " + str(
        x['dosage']) + " Side Effects: " + str(
        x['sideEffects'])), medication_list)


def get_next_medications_to_be_taken(medication_plans):
    medications = []
    now = datetime.now()
    for plan in medication_plans:
        intake_interval_first = plan['intakeIntervals'].split(' ')[0]
        intake_interval_second = plan['intakeIntervals'].split(' ')[1]
        date_time_first_obj = datetime.strptime(intake_interval_first, '%H:%M:%S')
        date_time_second_obj = datetime.strptime(intake_interval_second, '%H:%M:%S')
        current_time = datetime.strptime(str(now.hour) + ":" + str(now.minute) + ":" + str(now.second), '%H:%M:%S')
        if date_time_first_obj < current_time < date_time_second_obj:
            medications += plan['medications']
    return medications


def get_not_taken_medications(medication_plans):
    medications = []
    now = datetime.now()
    for plan in medication_plans:
        intake_interval_second = plan['intakeIntervals'].split(' ')[1]
        date_time_second_obj = datetime.strptime(intake_interval_second, '%H:%M:%S')
        current_time = datetime.strptime(str(now.hour) + ":" + str(now.minute) + ":" + str(now.second), '%H:%M:%S')
        if current_time > date_time_second_obj:
            medications += plan['medications']
    return medications


def find_medication_plan_for_medication_id(medication_plans, medication_id):
    for plan in medication_plans:
        for medication in plan['medications']:
            if medication['id'] == medication_id:
                return plan
    return None


def remove_medication_from_plans(medication_plans, medication_id):
    for plan in medication_plans:
        for medication in plan['medications']:
            if medication['id'] == medication_id:
                plan['medications'].remove(medication)
    return medication_plans
