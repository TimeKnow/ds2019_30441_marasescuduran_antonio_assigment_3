package utcn.MediPlatform.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import utcn.MediPlatform.dto.NotificationCaregiverDTO;
import utcn.MediPlatform.dto.NotificationDTO;
import utcn.MediPlatform.entities.MediUser;
import utcn.MediPlatform.entities.Notification;
import utcn.MediPlatform.event.NotificationCreatedEvent;
import utcn.MediPlatform.exception.ObjectNotFoundException;
import utcn.MediPlatform.repository.RepositoryFactory;

@Service
@RequiredArgsConstructor
public class NotificationManagementService {
    private final RepositoryFactory repositoryFactory;
    private final ApplicationEventPublisher eventPublisher;

    @Transactional
    public void createNotification(NotificationDTO notificationDTO) {
        MediUser patient = repositoryFactory.createUserRepository().findById(notificationDTO.getPatientId()).orElseThrow(ObjectNotFoundException::new);
        Notification notification = new Notification(null, patient, notificationDTO.getStart(),
                notificationDTO.getEnd(), notificationDTO.getActivity(), notificationDTO.getBrokenRules());
        repositoryFactory.createNotificationRepository().save(notification);
        if (notificationDTO.getBrokenRules() != null) {
            String message = "Patient " + notificationDTO.getPatientId() + " has broken the following rules: " + notificationDTO.getBrokenRules();
            System.out.println(message);
            NotificationCaregiverDTO notificationCaregiverDTO = new NotificationCaregiverDTO(notificationDTO.getPatientId(), message);
            eventPublisher.publishEvent(new NotificationCreatedEvent(notificationCaregiverDTO));
        }
    }
}
