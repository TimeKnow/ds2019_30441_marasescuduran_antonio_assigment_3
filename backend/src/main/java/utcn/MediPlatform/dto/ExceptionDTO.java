package utcn.MediPlatform.dto;

import lombok.Data;

@Data
public class ExceptionDTO {
    private final String message;
}
