package utcn.MediPlatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import utcn.MediPlatform.entities.MediUser;

import java.util.Optional;

public interface MediUserRepository extends JpaRepository<MediUser, Integer>, CRUDRepository<MediUser> {
    Optional<MediUser> findByEmail(String email);
}
