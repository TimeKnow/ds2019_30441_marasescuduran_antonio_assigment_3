package utcn.MediPlatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import utcn.MediPlatform.entities.MedicationPlanPillbox;

public interface MedicationPlanPillboxRepository extends JpaRepository<MedicationPlanPillbox, Integer> {
}
