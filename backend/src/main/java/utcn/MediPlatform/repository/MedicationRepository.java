package utcn.MediPlatform.repository;

import org.springframework.data.repository.Repository;
import utcn.MediPlatform.entities.Medication;

public interface MedicationRepository extends Repository<Medication, Integer>, CRUDRepository<Medication> {
}
