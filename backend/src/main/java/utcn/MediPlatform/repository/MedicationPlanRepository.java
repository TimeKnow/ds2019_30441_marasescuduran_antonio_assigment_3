package utcn.MediPlatform.repository;

import org.springframework.data.repository.Repository;
import utcn.MediPlatform.entities.MedicationPlan;

public interface MedicationPlanRepository extends Repository<MedicationPlan, Integer>, CRUDRepository<MedicationPlan> {
}
