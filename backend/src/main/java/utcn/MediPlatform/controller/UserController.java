package utcn.MediPlatform.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import utcn.MediPlatform.dto.UserAccountDTO;
import utcn.MediPlatform.service.UserSessionManagementService;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserSessionManagementService userSessionManagementService;

    @GetMapping("/api/me")
    public UserAccountDTO getCurrentUser() {
        return userSessionManagementService.loadCurrentUser();
    }
}
