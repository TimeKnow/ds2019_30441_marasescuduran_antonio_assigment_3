package utcn.MediPlatform.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;
import utcn.MediPlatform.event.BaseEvent;

@RestController
@RequiredArgsConstructor
public class WebSocketController {
    private final SimpMessagingTemplate messagingTemplate;

    @EventListener(BaseEvent.class)
    public void handleEvent(BaseEvent event) {
        messagingTemplate.convertAndSend("/topic/notifications", event);
    }
}
