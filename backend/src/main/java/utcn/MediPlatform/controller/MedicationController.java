package utcn.MediPlatform.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import utcn.MediPlatform.dto.FindByIdsInputDTO;
import utcn.MediPlatform.dto.MedicationDTO;
import utcn.MediPlatform.dto.MedicationPlanDTO;
import utcn.MediPlatform.service.MedicationManagementService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MedicationController {
    private final MedicationManagementService medicationManagementService;

    @GetMapping("/api/medications")
    public List<MedicationDTO> readAllMedications() {
        return medicationManagementService.getAllMedications();
    }

    @GetMapping("/api/medications/{id}")
    public MedicationDTO readMedication(@PathVariable("id") Integer id) {
        return medicationManagementService.getMedicationById(id);
    }

    @PostMapping("api/medications/find-by-ids")
    public List<MedicationDTO> readMedicationByIds(@RequestBody FindByIdsInputDTO findByIdsInputDTO) {
        return medicationManagementService.getAllMedicationByIds(findByIdsInputDTO);
    }

    @PostMapping("/api/medications")
    public MedicationDTO createMedication(@RequestBody MedicationDTO medicationDTO) {
        return medicationManagementService.createMedication(medicationDTO);
    }

    @PutMapping("/api/medications/{id}")
    public MedicationDTO updateMedication(@PathVariable("id") Integer id, @RequestBody MedicationDTO medicationDTO) {
        return medicationManagementService.updateMedication(id, medicationDTO);
    }

    @DeleteMapping("/api/medications/{id}")
    public void deleteMedication(@PathVariable("id") Integer id) {
        medicationManagementService.deleteMedication(id);
    }

    @GetMapping("api/medications/plans")
    public List<MedicationPlanDTO> readAllMedicationPlans() {
        return medicationManagementService.getAllMedicationPlans();
    }

    @GetMapping("api/medications/plans/patient/{id}")
    public List<MedicationPlanDTO> readAllMedicationPlans(@PathVariable("id") Integer id) {
        return medicationManagementService.getAllMedicationPlansOfPatient(id);
    }

    @PostMapping("api/medications/plans")
    public MedicationPlanDTO createMedicationPlan(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        return medicationManagementService.createMedicationPlan(medicationPlanDTO);
    }
}
