package utcn.MediPlatform.exception;

public class InvalidRequestContent extends RuntimeException {
    public InvalidRequestContent() {
        super("The request was invalid!");
    }
}
