package utcn.MediPlatform.exception;

public class ObjectAlreadyExistsException extends RuntimeException {
    public ObjectAlreadyExistsException() {
        super("The object already exists!");
    }
}
